from flask import Flask, request, redirect, render_template
from dirdict import DirDict
from base64 import b64encode
from string import ascii_letters
from os import getenv
from urllib.parse import urlparse

BASE_URL = getenv("BASE_URL")
if not BASE_URL:
    raise EnvironmentError("Environment variable BASE_URL must be set")

DEBUG = getenv("DEBUG", False)

# create it like this (default path is ".")
dirr = DirDict(getenv("DATA_DIRECTORY", "/data"))


app = Flask(__name__)


@app.route("/", methods=["GET"])
def index():
    return render_template("index.html")


def i_to_short(i):
    short = ""
    if i == 0:
        return ascii_letters[0]
    while i != 0:
        short += ascii_letters[i % len(ascii_letters)]
        i = i // len(ascii_letters)

    return short[::-1]


@app.route("/create", methods=["POST"])
def create():
    from_curl = request.user_agent.string.startswith("curl")

    url_raw = request.form.get("url", "").strip()
    if url_raw == "":
        if from_curl:
            return "Invalid url\n"
        else:
            return render_template("index.html"), 400
    to_url = urlparse(url_raw, scheme="https").geturl()
    # increment iterator
    dirr["iterator"] = str(int(dirr.get("iterator", b"0").decode()) + 1).encode()
    # get short url
    i = i_to_short(int(dirr["iterator"].decode()))
    # assign short url in dict to the actual url
    (dirr / "urls")[i] = to_url.encode()
    url = f"{BASE_URL}/{i}"
    if from_curl:
        return url+'\n'
    else:
        return render_template("index.html", url=url)


@app.route("/<short>")
def url(short):
    try:
        # find the real url from the short url
        return redirect((dirr / "urls")[str(short)])
    except:
        return "no found 🤷", 404


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=DEBUG)
